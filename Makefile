CC			=	gcc

DFLAGS  	= 	-g3

CFLAGS		=	-fPIC -W  -Wall -Werror

LDFLAGS 	= 	-shared

LIB_NAME 	= 	libmy_malloc_$(shell uname).so

TARGET_LIB  = 	lib

SRC			=	src/malloc.c src/free.c src/calloc.c src/helper.c

OBJ			=	$(SRC:%.c=%.o)

DB_MALLOC 	= 	debug_malloc_bin

SLINK		= 	symbolic_links

LINK 		= 	libmy_malloc.so

RM 			= 	rm -f

.PHONY		:	fclean clean all re

all			:	$(TARGET_LIB) $(SLINK) exp_lib

$(TARGET_LIB)	: 	$(OBJ)
	$(CC) $(LDFLAGS) -o $(LIB_NAME) $^


$(DB_MALLOC)	:	$(OBJ)
	$(CC) $(CFLAGS) $(DFLAGS) $(SRC) -o $@

$(SLINK)	:	$(LIB_NAME) 
	ln -s $(LIB_NAME) $(LINK)

exp_lib		:	$(LIB_NAME)
	export LD_PRELOAD=$(PWD)/$(LIB_NAME)

unset_lib	:	$(LINK)
	unset LD_PRELOAD

del_link	:	$(LIB_NAME)
	unlink $(LIB_NAME)
	rm $(LINK)

clean		:
	$(RM) $(OBJ) *.swp *~ *.o

fclean		:	clean unset_lib del_link
	$(RM) $(TARGET_LIB) $(DB_MALLOC) $(LIB_NAME)

re			:	fclean all
