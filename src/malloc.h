#ifndef MALLOC_H
# define MALLOC_H

# define _GNU_SOURCE
# include <sys/mman.h>
# include <unistd.h>
# include <errno.h>
# include <stdio.h>

# define PG_SIZE sysconf(_SC_PAGE_SIZE)
# define ALIGN(size) ((size + PG_SIZE - 1) & ~(PG_SIZE - 1))
# define FREE 0
# define MAPPED 2
# define FREED  3
# define ALLOC  1


/* Double Linked List to store the metadata
   and loop over the dynamically loaded */
struct n_ode
{
  int state;
  size_t size;
  struct n_ode *next;
  struct n_ode *prev;
};

// HELPER
int cmp(int x, int y);
void *move_ptr(void *p, size_t size);
struct n_ode *get_header(struct n_ode *r);

void *malloc(size_t size);
void free(void *ptr);

#endif /* MALLOC_H */
