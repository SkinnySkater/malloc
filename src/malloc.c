#include "malloc.h"


struct n_ode *search_bestfit(size_t size)
{
  struct n_ode *h;
  struct n_ode *best;

  h = get_header(NULL);
  best = NULL;
  while (h)
  {
    if (h->state == FREE || h->state == 20)
      // if best than the current one select it like algo of get max
      if (best == NULL || (h->size - size) < (best->size - size))
        best = h;
    h = h->next;
  }

  return (best);
}

struct n_ode *create_new_node(struct n_ode *next, size_t size)
{
  size_t align_sz;
  struct n_ode *n;

  align_sz = ALIGN(size + sizeof(struct n_ode));
  n = mmap(NULL, align_sz, PROT_READ
   | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
  if ((errno = ENOMEM) && (n == MAP_FAILED))
    return (NULL);
  n->state = MAPPED;
  n->size = (size + sizeof (struct n_ode) + sizeof(size_t) - 1)
   & ~(sizeof(size_t) - 1);
  n->prev = NULL;
  n->next = next;
  // Create a new structure if enough space
  if (!cmp(align_sz, (ALIGN(size + (2 * sizeof(struct n_ode))))))
  {
    n->next = move_ptr(n, n->size);;
    n->next->state = FREE;
    n->next->size = align_sz - n->size;
    n->next->prev = n;
    n->next->next = next;
    if (next)
      next->prev = n->next;
  }

  return (n);
}

void *malloc(size_t size)
{
  struct n_ode *n;
  struct n_ode *f;
  struct n_ode *t;

  n = NULL;
  f = search_bestfit(size);
  if (f == NULL || size > (unsigned)PG_SIZE)
  {
    f = get_header(NULL);
    t = create_new_node(f, size);
    n = get_header(t);
  }
  else
  {
    f->state = !cmp(f->state, FREED) ? MAPPED : ALLOC;
    n = f;
  }
  if (n == NULL)
    return (n);

  return (move_ptr(n, sizeof(struct n_ode)));
}