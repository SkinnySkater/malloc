#include "malloc.h"

void free(void *ptr)
{
  struct n_ode *n;
  if (ptr == NULL)
    return;
  n = move_ptr(ptr, -sizeof(struct n_ode));
  if (n->state == ALLOC)
  {
    if (n->prev && n->prev->state == FREED)
      n->prev->size += n->size;
    if (n->prev && n->prev->state == FREED && n->next)
      n->next->prev = n->prev;
  }
  else if (n->state == MAPPED)
  {
    while (n->next && n->next->state == FREE)
      n->next = n->next->next;
    if (n->next && (n->next->state == FREED || n->next->state == MAPPED))
    {
      if ((get_header(NULL) == n) && get_header(n->next))
        munmap(n, ALIGN(n->size));
      else if ((n->prev->next = n->next) && (n->next->prev = n->prev))
        munmap(n, ALIGN(n->size));
    }
    else
      n->state = FREED;
  }
}