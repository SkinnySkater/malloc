#include "malloc.h"

void *calloc(size_t nb, size_t size)
{
  size_t sz;
  struct node *n;

  if (nb == 0 || size == 0)
    return (NULL);
  sz = size * nb;
  n = malloc(sz);
  char *c = move_ptr(n, 0);
  while (sz != 0)
  {
    c = 0;
    c = move_ptr(c, sizeof(char));
    sz -= sizeof (char);
  }
  return (n);
}