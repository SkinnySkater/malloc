#include "malloc.h"


/*Get the header at top of the list*/
struct n_ode *get_header(struct n_ode *r)
{
  struct n_ode *head;

  head = NULL;
  if (r != NULL)
    head = r;

  return (head);
}

int cmp(int x, int y) {
    return (x = x - y, ( x >> 31 ) | !!x);
}

/*Increment pointer by type casting it with char */
void *move_ptr(void *p, size_t size)
{
  char *c;

  if (p == NULL)
    return NULL;
  c = p;
  c += size;

  return (c);
}