#FR
#Algorythme
** L’algorithme Best feet ** est implémenté car il aide beaucoup en donnant le meilleur noeud approprié pour héberger un nouveau
données mappées, augmentez ainsi légèrement la vitesse du code d’exécution.
En bref, il trouvera la taille appropriée la plus proche de la taille donnée.

Lors de la libération, vérifiez si les voisins ont été réinitialisés et liés les uns aux autres, sinon passez en boucle dans la liste et et
obtenir le dernier enfant libérant littéralement le noeud en appelant munmap et en mettant à jour le lien précédent précédent et lié le précédent de
actuel au précédent du dernier enfant.

#À propos de la mise en œuvre
Les données sont alignées, pourquoi?
Parce que l'alignement de la structure de données fait référence à la façon dont les données sont organisées et accessibles dans la mémoire de l'ordinateur.
Le processeur du matériel informatique moderne lit et écrit en mémoire le plus efficacement possible lorsque le
les données sont naturellement alignées, ce qui signifie généralement que l'adresse des données est un multiple de la taille des données.
Rembourrage informatique:
Les formules suivantes fournissent le nombre d’octets de remplissage requis pour aligner le début de
 une structure de données (où mod est l'opérateur modulo):

padding = (align - (offset mod align)) mod aligner
aligné = décalage + remplissage
        = offset + ((align - (offset mod align)) mod aligner)

Puisque l’alignement est par définition une puissance de deux, l’opération modulo peut être réduite
à un bitwise ET opération.

Les formules suivantes produisent le décalage aligné (où & est un bit AND et ~ un bit NOT):

padding = (align - (offset & (align - 1))) & (align - 1)
        = (-offset & (align - 1))
aligné = (décalage + (aligner - 1)) & ~ (aligner - 1)
        = (offset + (align - 1)) & -align
Voir https://en.wikipedia.org/wiki/Data_structure_alignment pour plus d'informations.


#Structure de données
L'implémentation est alimentée par une double liste chaînée.
Ce lien permet de gérer correctement la libération des données et d’éviter le gaspillage de noeuds, aussi appelé fragmentation.

#Syscalls
L'implémentation utilise 3 appels système:
-long sysconf (int name) -> pour obtenir des constantes sur le système, ici dans ce cas
nous l'appelons pour obtenir la taille de la page du système actuel
La fonction -mmap doit établir un mappage entre l'espace d'adressage d'un processus et un fichier, un objet de mémoire partagée ou un objet de mémoire typé. Le format de l'appel est le suivant
void * mmap (void * addr, longueur_taille, intprot, drapeaux int, int fd, off_t offset);
Indicateur PROT_WRITE Des données peuvent être écrites.
flag MAP_PRIVATE Les modifications sont privées.
MAP_ANONYMOS Le mappage n'est sauvegardé par aucun fichier. son contenu est initialisé à zéro
L'appel système -munmap supprime les mappages pour la plage d'adresses spécifiée et entraîne la génération d'autres références aux adresses dans la plage
références de mémoire non valides. La région est également automatiquement désaffectée lorsque le processus est terminé.
La fonction void * calloc (size_t nitems, size_t size) alloue la mémoire demandée et renvoie un pointeur sur celle-ci.
Nous utilisons un pointeur de caractère pour incrémenter le pointeur vide puis décrémenter la taille jusqu'à zéro jusqu'à ce que
addrss est défini sur 0.

#US
#Algorythm
**The Best feet** Algorithm is implemented cause it helps a lot giving the best suitable node to host a new
mapped data, thus increase slightly the velocity of the execution code.
In short, it will find the appropriate size which is closer to the given size.	

While freeing , check if neightboors has been reset and bound the previous to the next , if not loop through the list and and
get the last child  to literaly free the node by calling munmap and updating the previous next link and bound the prev of
current to previous of last child.

#About The implementation
Data are aligned, Why ?
Because Data structure alignment refers to the way data is arranged and accessed in computer memory.
The CPU in modern computer hardware performs reads and writes to memory most efficiently when the 
data is naturally aligned, which generally means that the data address is a multiple of the data size.
Computing padding:
The following formulas provide the number of padding bytes required to align the start of
 a data structure (where mod is the modulo operator):

padding = (align - (offset mod align)) mod align
aligned = offset + padding
        = offset + ((align - (offset mod align)) mod align)

Since the alignment is by definition a power of two, the modulo operation can be reduced 
to a bitwise boolean AND operation.

The following formulas produce the aligned offset (where & is a bitwise AND and ~ a bitwise NOT):

padding = (align - (offset & (align - 1))) & (align - 1)
        = (-offset & (align - 1))
aligned = (offset + (align - 1)) & ~(align - 1)
        = (offset + (align - 1)) & -align
See https://en.wikipedia.org/wiki/Data_structure_alignment for more


#Data Structure
The implementation is powered by a double Linked List.
this link help to manage freeing the data corectly and avoid waste of nodes thus so called fragmentation.

#Syscalls
Implementation is using 3 syscalls:
	-long sysconf(int name) --> to get some constants about system, here in this case 
	we call it to get the current system's page size
	-mmap function shall establish a mapping between a process' address space and a file, shared memory object, or typed memory object. The format of the call is as follow
	void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
	flag PROT_WRITE 	Data can be written.
	flag MAP_PRIVATE 	Changes are private.
	MAP_ANONYMOS   		The mapping is not backed by any file; its contents are initialized to zero
	-munmap system call deletes the mappings for the specified address range, and causes further references to addresses within the range to generate
	 invalid memory references. The region is also automatically unmapped when the process is terminated.
The function void *calloc(size_t nitems, size_t size) allocates the requested memory and returns a pointer to it.
We use a char pointer to increment the void pointer then succesively decrement the size until zero while all
addrss is set to 0.
